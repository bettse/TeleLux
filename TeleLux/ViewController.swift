//
//  ViewController.swift
//  TeleLux
//
//  Created by Eric Betts on 7/4/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, BLEDelegate {
    @IBOutlet weak var statusLabel : UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sectorRingView: SegmentedRingView!
    let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext

    let ble = BLE()
    
    var patterns : [Pattern] = []
    var live = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ble.delegate = self
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        let fetchRequest = NSFetchRequest(entityName: "Pattern")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        do {
            self.patterns = try self.managedObjectContext.executeFetchRequest(fetchRequest) as! [Pattern]
        } catch {
            print("Error fetching Patterns from CoreData")
            
        }
        
        //A starting point
        if (self.patterns.count == 0) {
            print("No patterns found in core data, populating with basic list")
            self.patterns = Pattern.basic()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func bleDidUpdateState() {
        ble.startScanning(60.0)
        statusLabel.text = "Scanning"
    }
    
    func bleDidConnectToPeripheral() {
        statusLabel.text = "Connected"
    }
    
    func bleDidDisconenctFromPeripheral() {        
        statusLabel.text = "Disconnected"
        NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(self.bleDidUpdateState), userInfo: nil, repeats: false)
    }
    
    func bleDidReceiveData(data: NSData?) {
    }
    
    @IBAction func segmentedAction(sender: UISegmentedControl) {
        print("segmentedAction \(sender.selectedSegmentIndex)")
        switch (sender.selectedSegmentIndex) {
        case 0: //Save
            let commands : [Command] = sectorRingView.active.map({ return Command.One($0, r: 0xFF, g: 0xFF, b: 0xFF) })
            let alert = UIAlertController(title: "Save", message: "Name your pattern", preferredStyle: .Alert)
            let alertAction = UIAlertAction(title: "OK!", style: .Default) { (UIAlertAction) -> Void in
                let name = alert.textFields?.first?.text ?? "temp"
                Pattern.build(name, commands: Set(commands))
            }
            alert.addAction(alertAction)
            alert.addTextFieldWithConfigurationHandler(nil)
            presentViewController(alert, animated: true, completion: nil)
        case 1: //Test
            for sector in sectorRingView.active {
                let c = Command.One(sector, r: 0xFF, g: 0xFF, b: 0xFF)
                ble.write(data: c.serialize())
            }
        case 2: //Live
            self.live = true
        default:
            print("Unhandled option")
        }
    }
    
    @IBAction func selected(sectorRingView : SegmentedRingView) {
        print("selected \(sectorRingView.active)")
        if live {
            for sector in sectorRingView.active {
                let c = Command.One(sector, r: 0xFF, g: 0xFF, b: 0xFF)
                ble.write(data: c.serialize())
            }
        }
    }
}

extension ViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let pattern = self.patterns[indexPath.row]
        print("Selected \(pattern.name)")
        for command in pattern.commands {            
            ble.write(data: (command as! Command).serialize()) //Need delay when there is more than one command
        }
    }
}

extension ViewController : UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.patterns.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let pattern = self.patterns[indexPath.row]
 
        let cell : UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("cell")! as UITableViewCell
        cell.textLabel?.text = pattern.name
        
        return cell
    }
}