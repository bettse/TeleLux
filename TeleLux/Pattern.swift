//
//  Pattern.swift
//  TeleLux
//
//  Created by Eric Betts on 7/6/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import Foundation
import CoreData
import UIKit

/*
 * Protocol:
 * 'A', R, G, B = Set all to RGB
 * 'O', N, R, G, B = Set index N to RGB
 * 'R', S = Set rotate with speed S
 */

class Pattern : NSManagedObject {
    static let Extension = "ptn"
    static let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    @NSManaged var name : String
    @NSManaged var commands : NSOrderedSet
    
    var filename : String {
        get {
            return name + "." + Pattern.Extension
        }
    }

    static func build(name: String, commands: Set<Command>) -> Pattern {
        let p = NSEntityDescription.insertNewObjectForEntityForName("Pattern", inManagedObjectContext: self.managedObjectContext) as! Pattern
        p.name = name
        p.commands = NSOrderedSet(set: commands)
        return p
    }
    
    static func basic() -> [Pattern] {
        return [
            Pattern.build("All Red", commands: Set([Command.All(0xFF, g: 0x00, b: 0x00)])),
            Pattern.build("All Green", commands: Set([Command.All(0x00, g: 0xFF, b: 0x00)])),
            Pattern.build("All Blue", commands: Set([Command.All(0x00, g: 0x00, b: 0xFF)])),
            Pattern.build("All Yellow", commands: Set([Command.All(0xFB, g: 0xFC, b: 0x5E)])),
            Pattern.build("All White", commands: Set([Command.All(0xFF, g: 0xFF, b: 0xFF)])),
            Pattern.build("All Off", commands: Set([Command.All(0x00, g: 0x00, b: 0x00)])),
        ]
    }
}