//
//  SegmentedRingView.swift
//  TeleLux
//
//  Created by Eric Betts on 7/8/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import UIKit

@IBDesignable class SegmentedRingView : UIControl {
    @IBInspectable var borderColor: UIColor = UIColor.blackColor()
    @IBInspectable var borderWidth: CGFloat = 1.0
    @IBInspectable var sectorCount: Int = 1
    @IBInspectable var segmentWidth: CGFloat = 48.0
    
    let pi = CGFloat(M_PI)
    
    var arc : CGFloat {
       return 360.0/CGFloat(sectorCount)
    }
    
    var radius : CGFloat {
        return bounds.size.width/2
    }
    
    var innerView : UIView {
        let view = UIView(frame: CGRectMake(segmentWidth/2, segmentWidth/2, self.frame.size.width - segmentWidth, self.frame.size.height - segmentWidth))
        view.layer.cornerRadius = view.bounds.size.width/2
        view.clipsToBounds = true
        return view
    }
    
    var active : [Int] {
        return selectedSectors.keys.sort()
    }
    var selectedSectors : [Int:Bool] = [:]
    var touchedSectors : [Int] = []
    
    override internal func layoutSubviews() {
        super.layoutSubviews()
        backgroundColor = UIColor.clearColor()        
        layer.cornerRadius = bounds.size.width/2
        clipsToBounds = true
        addSubview(innerView)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        touchedSectors = []
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let origin = CGPoint(x: radius, y: radius)
        for touch in touches {
            let point = touch.locationInView(self)
            let adjustedPoint = CGPoint(x: point.x - origin.x, y: point.y - origin.y)
            let sector = Int(floor(adjustedPoint.ϕ/arc))
            let bounds = (radius - segmentWidth)..<radius
            if bounds.contains(adjustedPoint.r) {
                if (!touchedSectors.contains(sector)) {
                    touchedSectors.append(sector)
                    selectedSectors[sector] = !(selectedSectors[sector] ?? false)
                    setNeedsDisplay()
                }
            }
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        //print(selectedSectors)
        self.sendActionsForControlEvents(UIControlEvents.ValueChanged)
    }

    
    override func drawRect(rect:CGRect) {
        super.drawRect(rect)
        let rotate = pi / 180 * arc
        let context = UIGraphicsGetCurrentContext()
        let rad = CGRectGetWidth(rect)/2
        
        //outer circle
        CGContextAddArc(context, CGRectGetMidX(rect), CGRectGetMidY(rect), rad, 0, 2*pi, 1) //Swift3.0: CGFloat.pi
        CGContextSetStrokeColorWithColor(context, borderColor.CGColor)
        CGContextSetLineWidth(context, borderWidth)
        CGContextStrokePath(context)
        
        //inner circle
        CGContextAddArc(context, CGRectGetMidX(rect), CGRectGetMidY(rect), rad - segmentWidth, 0, 2*pi, 1)
        CGContextSetStrokeColorWithColor(context, borderColor.CGColor)
        CGContextSetLineWidth(context, borderWidth)
        CGContextStrokePath(context)
        

        if (sectorCount > 1) { //0 segments = full loop; 1 segment, also a full loop
            CGContextSaveGState(context)
            CGContextTranslateCTM(context, CGRectGetMidX(rect), CGRectGetMidY(rect))
            
            //Fill in active sectors
            for i in 0..<sectorCount {
                if (selectedSectors[i] ?? false) {
                    CGContextSetStrokeColorWithColor(context, UIColor.grayColor().CGColor)
                    CGContextAddArc(context, 0, 0, radius, 0, CGFloat(DegreesToRadians(Double(arc))), 0)
                    CGContextSetLineWidth(context, segmentWidth*2)
                    CGContextDrawPath(context, CGPathDrawingMode.Stroke)
                    CGContextStrokePath(context)
                }
                CGContextRotateCTM(context, rotate)
            }
            
            
            //Spokes between sectors
            for _ in 0..<sectorCount {
                CGContextRotateCTM(context, rotate)
                let path = CGPathCreateMutable()
                CGPathMoveToPoint(path, nil, rad - segmentWidth, 0)
                CGPathAddLineToPoint(path, nil, rad, 0)
                CGContextAddPath(context, path)
                CGContextSetStrokeColorWithColor(context, layer.borderColor)
                CGContextSetLineWidth(context, borderWidth)
                CGContextStrokePath(context)
            }
            
            CGContextRestoreGState(context)
        }
    }
    
    func DegreesToRadians (value:Double) -> Double {
        return value * M_PI / 180.0
    }
}



