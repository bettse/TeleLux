//
//  Command.swift
//  TeleLux
//
//  Created by Eric Betts on 7/6/16.
//  Copyright © 2016 Eric Betts. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Command : NSManagedObject {
    static let managedObjectContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    @NSManaged var type : NSData
    @NSManaged var parameters : NSData
    @NSManaged var pattern : Pattern
    
    func serialize() -> NSData {
        let s = NSMutableData(data: self.type)
        s.appendData(self.parameters)
        return NSData(data: s)
    }
    
    static func All(r: UInt8, g: UInt8, b: UInt8) -> Command {
        let c = NSEntityDescription.insertNewObjectForEntityForName("Command", inManagedObjectContext: self.managedObjectContext) as! Command
        c.type = NSData(bytes: ["A".asciiValue] as [UInt8], length: 1)
        c.parameters = NSData(bytes: [r, g, b] as [UInt8], length: 3)
        return c
    }
    
    static func One(index: Int, r: UInt8, g: UInt8, b: UInt8) -> Command {
        let c = NSEntityDescription.insertNewObjectForEntityForName("Command", inManagedObjectContext: self.managedObjectContext) as! Command
        c.type = NSData(bytes: ["O".asciiValue] as [UInt8], length: 1)
        c.parameters = NSData(bytes: [UInt8(index), r, g, b] as [UInt8], length: 4)
        return c
    }
    
}